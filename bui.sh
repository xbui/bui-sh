#!/bin/bash -p
# Functions and aliases used to nagivate project directories.
# Include by the ~/bin/cdtools script.
#############################################################################

# -------------------------------------------------------------------
# BUI: Beagle UI, a set top box UI base on X11
# -------------------------------------------------------------------
function bui {

    # Project ID
    # The first argument is the project and must be provided
    if [ "$1" != "" ]
    then
        PRJ=$1
    else
        echo "You must provide a project as the first argument"
        echo "Available projects:"
        echo "fakekey"
        echo "keyboard"
        echo "lib"
        echo "panel"
        echo "wm"
        echo "network-config"
        return;
    fi

    # -------------------------------------------------------------------
    # Edit these items
    # -------------------------------------------------------------------

    USER_NAME="<your name here>"
    USER_EMAIL="<your email here>"

    # Top of the source tree
    # The source, build, archive and package directories will live
    # under this tree for all BUI repositories.
    SRCTOP=~/src/ximba/bui

	# For cross compiling (ignored for native compiles):
    # Use prepackaged version of cross toolchain
    # XCC_DIR=/opt/beagleboxTC
    XCC_DIR=/opt/rpiTC

    # -------------------------------------------------------------------
    # Don't edit below here
    # -------------------------------------------------------------------

    # -------------------------------------------
    # Gitorious config
    # GIT Repo
    export GITREPO=git@gitorious.org/bui/bui-$PRJ.git
    # -------------------------------------------

    # Where I do my dev work
    GM_WORK=$SRCTOP/work

    # Where the SCM is located
    GM_HOME=$SRCTOP/$PRJ

    # Where the source and build directories live
    GM_SRC=$GM_HOME/src
    GM_BUILD=$GM_HOME/bld
    GM_ARCHIVE=$GM_HOME/archive

    # Make the configured environment available to the build system.
    export GM_WORK
    export GM_ARCHIVE
    export GM_HOME
    export GM_SRC
    export GM_BUILD
    export GM_TOOLS

    # Some aliases to bounce around directories easily
    alias cdt='cd $SRCTOP'
    alias cdw='cd $GM_WORK'
    alias cdx='cd $GM_SRC'
    alias cdb='cd $GM_BUILD'
    alias cda='cd $GM_ARCHIVE'
    alias cdh='cd $GM_HOME'

    # Show the aliases for this configuration
    alias cd?='cdbui?'
    alias cvsu='cvs update 2>&1 | egrep "^[UPAMRC\?]"'
}
function cdbui? {
echo "
BUI Alias settings:
-----------------------------------------------------------------------------
cdt    cd SRCTOP ($SRCTOP)
cdh    cd GM_HOME ($GM_HOME)
cdw    cd GM_WORK ($GM_WORK)
cdx    cd GM_SRC ($GM_SRC)
cdb    cd GM_BUILD ($GM_BUILD)
cda    cd GM_ARCHIVE ($GM_ARCHIVE)

GITREPO     : $GITREPO
XCC_DIR     : $XCC_DIR

To checkout tree:
cdt
mkdir $PRJ
cdh
git clone ssh://$GITREPO src

Pushing your local repository to Gitorious:
    cd <src>
    git init
    git add .
    git commit
    git checkout master
    git remote add origin git@gitorious.org:bui/bui-$PRJ.git
    git push origin master
"
}

